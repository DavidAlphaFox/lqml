Before applying this patch, please install latest ECL from development branch
(as of May 2023).

Just copy **float-features** from Quicklisp under
`~/quicklisp/local-projects/` and apply patch (or copy `float-features.lisp`).
